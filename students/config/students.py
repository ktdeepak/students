from __future__ import unicode_literals
from frappe import _


def get_data():
    return [
        {
            "label": _("Students"),
            "icon": "icon-star",
            "items": [
                {
                    "type": "doctype",
                    "name": "Student",
                    "description": _("Students"),
                },
            ]
        },
        {
            "label": _("Student Reports"),
            "icon": "icon-star",
            "items": [
                {
                    "type": "page",
                    "name": "students-summary",
                    "label": _("Students Summary"),
                    "description": _("Students Summary"),
                },
            ]
        }
    ]
