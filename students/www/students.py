import frappe

template_path = "www/students.html"

def get_context(context):
    context.students = frappe.get_all("Student", fields=['student_name', 'department', 'student_email'])
