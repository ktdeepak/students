import frappe

def get_context(context):
    context.students = frappe.get_all("Student", fields=['student_name', 'department', 'student_email'])
