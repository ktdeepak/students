# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and Contributors
# License: GNU General Public License v3. See license.txt

from __future__ import unicode_literals
import json

import frappe
from frappe import _


@frappe.whitelist()
def students_summary(gender="B"):
    if gender=="B":
        students_count = frappe.db.sql("""select department, count(*) as department_count from tabStudent group by  1 order by 1""",
                                       as_dict = True)
    else:
        gender_filter = "Male" if gender=="M" else "Female"

        students_count = frappe.db.sql("""select department, count(*) as department_count from tabStudent
        where gender='%s'
        group by  1 order by 1""" %(gender_filter),
                               as_dict = True)


    return json.dumps(students_count)

@frappe.whitelist()
def students_gender(gender="B"):
    if gender=="B":
        gender_summary = frappe.db.sql("""select gender, count(*) as gender_count from tabStudent group by 1 order by 1""",
                                       as_dict=True)
    else:
        gender_filter = "Male" if gender=="M" else "Female"
        gender_summary = frappe.db.sql("""select gender, count(*) as gender_count
        from tabStudent where gender='%s' group by 1 order by 1""" %(gender_filter),
                                       as_dict=True)

    return json.dumps(gender_summary)
