// Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and Contributors
// License: GNU General Public License v3. See license.txt


frappe.pages['students-summary'].on_page_load = function(wrapper) {

    students_summary_result = new Array();
    students_gender_result = new Array();
    students_summary_result.push(['Department', 'Department Count']);
    students_gender_result.push(['Gender', 'Gender Count']);


	frappe.breadcrumbs.add("Students");

		frappe.call({
			method: "students.students.page.students_summary.students_summary.students_summary",
			callback: function(r) {
				if(!r.exc) {
                  var students = JSON.parse(r.message);
                  students.forEach(function (student) {
                    students_summary_result.push([student.department, student.department_count]);
                  });
				}
			}
		});

		frappe.call({
			method: "students.students.page.students_summary.students_summary.students_gender",
			callback: function(r) {
				if(!r.exc) {
                  var students_gender = JSON.parse(r.message);
                  students_gender.forEach(function (student) {
                    students_gender_result.push([student.gender, student.gender_count]);
                  });
				}
			}
		});


	var mybody = document.getElementsByTagName('body')[0];

	mybody.innerHTML = frappe.templates["students_summary"] + mybody.innerHTML;
  draw_chart();

};

function draw_chart() {
        google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {

        var data_dept = google.visualization.arrayToDataTable(students_summary_result);

        var options_dept = {
          title: 'Students by department',
            is3D: true
        };

        var chart_dept = new google.visualization.PieChart(document.getElementById('chart-001'));

        chart_dept.draw(data_dept, options_dept);


        var data_gender = google.visualization.arrayToDataTable(students_gender_result);

        var options_gender = {
          title: 'Gender breakdown',
            is3D: true
        };

        var chart_gender = new google.visualization.PieChart(document.getElementById('chart-002'));

        chart_gender.draw(data_gender, options_gender);

      }
}
