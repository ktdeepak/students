// Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and Contributors
// License: GNU General Public License v3. See license.txt


frappe.pages['students-summary'].on_page_load = function(wrapper) {

	frappe.ui.make_app_page({
		parent: wrapper,
		title: __('Students Summary'),
		single_column: true
	});

    wrapper.students_summary = new StudentsSummary(wrapper);

	frappe.breadcrumbs.add("Students");
};


StudentsSummary = Class.extend({
	init: function(wrapper) {
		var me = this;
        window.base_student_class = me;
		// 0 setTimeout hack - this gives time for canvas to get width and height
		setTimeout(function() {
			me.setup(wrapper);
            me.init_chart();
		}, 0);
	},

	setup: function(wrapper) {
		var me = window.base_student_class;

		this.elements = {
			layout: $(wrapper).find(".layout-main"),
            layout_ms: $(wrapper).find(".layout-main-section"),
            gender_select: wrapper.page.add_select(__("Gender"), [
                {'label': 'Both Genders', value: 'B'},
                {'label': 'Male', 'value': 'M'},
                {'label': 'Female', 'value': 'F'}
            ]),
			refresh_btn: wrapper.page.set_primary_action(__("Refresh"),
				function() { me.get_data(); }, "icon-refresh")
		};

		this.elements.no_data = $('<div class="alert alert-warning">' + __("No Data") + '</div>')
			.toggle(false)
			.appendTo(this.elements.layout);

		this.elements.dept_chart = $('<div id="chart-001"></div>').appendTo(this.elements.layout_ms);
        this.elements.gender_chart = $('<div id="chart-002"></div>').appendTo(this.elements.layout_ms);


		// bind refresh
		this.elements.refresh_btn.on("click", function() {
			me.get_data();
		});

	},

    init_chart: function() {
        var me = window.base_student_class;
      google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(me.get_data);
    },

	get_data: function(btn) {
		var me = window.base_student_class;

		frappe.call({
			method: "students.students.page.students_summary.students_summary.students_summary",
            args: {
              gender: me.elements.gender_select[0].value
            },
            btn: btn,
			callback: function(r) {
                me.students_summary_result = [];
                me.students_summary_result.push(['Department', 'Department Count']);

				if(!r.exc) {
                  var students = JSON.parse(r.message);
                  students.forEach(function (student) {
                    me.students_summary_result.push([student.department, student.department_count]);
                  });
				}

                frappe.call({
                    method: "students.students.page.students_summary.students_summary.students_gender",
                    args: {
                      gender: me.elements.gender_select[0].value
                    },
                    btn: btn,
                    callback: function(r) {
                        me.students_gender_result = [];
                        me.students_gender_result.push(['Gender', 'Gender Count']);
                        if(!r.exc) {
                          var students_gender = JSON.parse(r.message);
                          students_gender.forEach(function (student) {
                            me.students_gender_result.push([student.gender, student.gender_count]);
                          });
                            me.draw_chart();
                        }
                    }
                });
			}
		});

	},

    draw_chart: function() {
        var me = window.base_student_class;
        var data_dept = google.visualization.arrayToDataTable(me.students_summary_result);

        var options_dept = {
          title: 'Students by department',
            is3D: true
        };

        var chart_dept = new google.visualization.PieChart(document.getElementById('chart-001'));

        chart_dept.draw(data_dept, options_dept);

        var data_gender = google.visualization.arrayToDataTable(me.students_gender_result);

        var options_gender = {
          title: 'Gender breakdown',
            is3D: true
        };

        var chart_gender = new google.visualization.PieChart(document.getElementById('chart-002'));

        chart_gender.draw(data_gender, options_gender);

    }

});
