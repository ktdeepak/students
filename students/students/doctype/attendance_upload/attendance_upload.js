// Copyright (c) 2016, Deepak and contributors
// For license information, please see license.txt

frappe.ui.form.on('Attendance Upload', {
	refresh: function(frm) {

	}
});

frappe.ui.form.on("Attendance Upload", "sync_attendance", function(frm) {
  frappe.call({
    method: "students.students.doctype.attendance_upload.attendance_upload.sync_attendance",
	  args: {name: frm.doc.name},
    callback: function(r) { }
  });
});
