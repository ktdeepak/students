# -*- coding: utf-8 -*-
# Copyright (c) 2015, Deepak and contributors
# For license information, please see license.txt

from __future__ import unicode_literals

from datetime import date
import os

import frappe
from frappe import _
from frappe.model.document import Document

from frappe.utils.background_jobs import enqueue


class AttendanceUpload(Document):
    def validate(self):
        if not self.upload_date:
            self.upload_date = date.today().isoformat()

        if self.attendance_file:
            file_path = os.path.join(os.getcwd(), frappe.local.site_path.split("/")[1])
            full_file_path = file_path + self.attendance_file


@frappe.whitelist()
def sync_attendance(name):
    print('*** You called me from ', name)

    attendance_set = frappe.get_doc("Attendance Upload", name)

    if attendance_set.sync_done:
        frappe.throw(_("Synchronization has already been staged. "
                       "Please check if that is done. If not, please"
                       "contact IT support to unstage."))

    attendance_set.sync_done = 1
    attendance_set.save()

    enqueue(async_worker, name=name)

    frappe.msgprint("Synchronization staged successfully!")


def async_worker(name):
    print("**** woo hoo ***", name)
    print("**** woo hoo ***")
    print("**** woo hoo ***", name)
    print("**** woo hoo ***")
    print("**** woo hoo ***", name)