# -*- coding: utf-8 -*-
# Copyright (c) 2015, Deepak and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from frappe.model.document import Document
from frappe.website.website_generator import WebsiteGenerator


class Student(WebsiteGenerator):
    website = frappe._dict(
        template = "templates/generators/student.html",
        page_name_field = "name",
        condition_field = "show_in_website",
        parent_website_route_field = "parent_website_route"
    )

    def validate(self):
        # check if student with  this email exists already. if so, push back with error
        student_list = [x['name'] for x in frappe.get_list("Student", fields=["name"],
                           filters={"student_email": self.student_email})]

        if self.name in student_list:
            pass
        elif not student_list:
            pass
        else:
            frappe.throw(_("Student with email %s already exists" % self.student_email))

        self.title = self.student_name + " - " + self.student_email

        # set page name the same as the document name
        self.page_name = self.name.lower()
